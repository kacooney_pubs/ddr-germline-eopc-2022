#!/bin/bash
base=/mnt/data1/workspace/Cooney/WES/Proc/decompression/
genome=/mnt/data1/workspace/Cooney/WES/Annotation/genome/
wdir=/mnt/data1/workspace/Cooney/WES/Proc/gatk_wdir/

singularity exec -B ${genome}:/genome/ -B ${wdir}:/opt/wdir/:rw -B ${base}:/data/:ro docker://broadinstitute/gatk:4.1.9.0 bash -c "gatk --java-options '-DGATK_STACKTRACE_ON_USER_EXCEPTION=true' VariantFiltration -V /opt/wdir/genotyped.vcf.gz --filter-expression 'ExcessHet > 54.69' --filter-name ExcessHet -OVI -O /opt/wdir/filtered.vcf.gz"
