#!/bin/bash
reference=/mnt/data1/workspace/Cooney/WES/Annotation/reference/
wdir=/mnt/data1/workspace/Cooney/WES/Proc/gatk_wdir/

singularity exec -B ${wdir}:/opt/wdir/:rw -B ${reference}:/reference/:ro docker://broadinstitute/gatk:4.1.9.0 bash -c 'gatk VariantRecalibrator -V /opt/wdir/sites.vcf.gz --trust-all-polymorphic -tranche 100.0 -tranche 99.95 -tranche 99.9 -tranche 99.5 -tranche 99.0 -tranche 97.0 -tranche 96.0 -tranche 95.0 -tranche 94.0 -tranche 93.5 -tranche 93.0 -tranche 92.0 -tranche 91.0 -tranche 90.0 -an FS -an ReadPosRankSum -an MQRankSum -an QD -an SOR -an InbreedingCoeff -mode INDEL --max-gaussians 4 -resource:mills,known=false,training=true,truth=true,prior=12.0 /reference/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz -resource:dbsnp,known=true,training=false,truth=false,prior=2.0 /reference/Homo_sapiens_assembly38.known_indels.vcf.gz -O /opt/wdir/indels.recal.vcf --tranches-file /opt/wdir/indels.tranches'
