#!/bin/bash
base=/mnt/data1/workspace/Cooney/WES/Proc/decompression/
reference=/mnt/data1/workspace/Cooney/WES/Annotation/reference/
genome=/mnt/data1/workspace/Cooney/WES/Annotation/genome/
wdir=/mnt/data1/workspace/Cooney/WES/Proc/gatk_wdir/

singularity exec -B ${genome}:/genome/ -B ${reference}:/reference/ -B ${wdir}:/opt/wdir/:rw -B ${base}:/data/:ro docker://broadinstitute/gatk:4.1.9.0 bash -c "gatk --java-options '-DGATK_STACKTRACE_ON_USER_EXCEPTION=true' GenotypeGVCFs --dbsnp /reference/Homo_sapiens_assembly38.dbsnp138.vcf -V gendb:///opt/wdir/db  -R /genome/genome.fasta --tmp-dir /tmp/ -O /opt/wdir/genotyped.vcf.gz"
