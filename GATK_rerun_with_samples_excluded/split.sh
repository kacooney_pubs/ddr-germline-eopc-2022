#!/bin/bash

wdir=/mnt/data1/workspace/Cooney/WES/Proc/gatk_wdir/

tabix -h ${wdir}/final.vcf.gz chr${1} | bgzip -c > ${wdir}/split/final-chr${1}.vcf.gz
tabix ${wdir}/split/final-chr${1}.vcf.gz
