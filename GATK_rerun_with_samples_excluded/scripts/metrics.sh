#!/bin/bash
reference=/mnt/data1/workspace/Cooney/WES/Annotation/reference/
genome=/mnt/data1/workspace/Cooney/WES/Annotation/genome/
wdir=/mnt/data1/workspace/Cooney/WES/Proc/gatk_wdir/

singularity exec -B ${wdir}:/opt/wdir/ -B ${reference}:/reference/ -B ${genome}:/genome/ docker://broadinstitute/picard bash -c 'java -jar /usr/picard/picard.jar CollectVariantCallingMetrics INPUT=/opt/wdir/final.vcf.gz DBSNP=/reference/Homo_sapiens_assembly38.dbsnp138.vcf SEQUENCE_DICTIONARY=/genome/genome.dict OUTPUT=/opt/wdir/variant_metrics.txt THREAD_COUNT=10 TARGET_INTERVALS=/genome/S31285117_Padded.interval_list'
