#!/bin/bash

wdir=/mnt/data1/workspace/Cooney/WES/Proc/gatk_wdir/

singularity exec -B ${wdir}:/opt/wdir/ docker://broadinstitute/picard bash -c 'java -jar /usr/picard/picard.jar MakeSitesOnlyVcf INPUT=/opt/wdir/filtered.vcf.gz OUTPUT=/opt/wdir/sites.vcf.gz'
