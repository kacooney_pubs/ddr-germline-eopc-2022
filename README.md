Directories:

AWS: Code used for AWS run

GATK_rerun_samples_excluded: scripts for running against GVCFs to final VCF excluding two samples

VEPAnno/VEP: Annotate the split final VCF

VEPAnno/merge/liftover: liftover hg38->hg19

VEPAnno/VEPAnno.R: QC (extract annotations, liftover, merge in REVEL scores) & split by BED file ref field