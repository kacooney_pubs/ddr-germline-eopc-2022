#!/bin/bash
base=/mnt/data1/workspace/Cooney/WES/Proc/gvcf/
genome=/mnt/data1/workspace/Cooney/WES/Annotation/genome/
wdir=/mnt/data1/workspace/Cooney/WES/Proc/gatk_wdir/

singularity exec -B ${genome}:/genome/ -B ${wdir}:/opt/wdir/:rw -B ${base}:/data/:ro docker://broadinstitute/gatk:4.1.9.0 bash -c "gatk --java-options '-DGATK_STACKTRACE_ON_USER_EXCEPTION=true -Xmx250G' GenomicsDBImport --genomicsdb-workspace-path /opt/wdir/db --sample-name-map /opt/wdir/sample.map -L /genome/S31285117_Padded.interval_list --tmp-dir /tmp/ --max-num-intervals-to-import-in-parallel 10 --merge-input-intervals"
