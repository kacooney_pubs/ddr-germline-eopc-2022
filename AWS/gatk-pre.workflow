---
apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: gatk-pre-
spec:
  entrypoint: gatk-pre

  arguments:
    parameters:
    - name: sample-list
      value: |
        [{"uid": "test", "R1": "test_R1.fastq.gz", "R2": "test_R2.fastq.gz"}]
    - name: bucket
      value: bucket
    - name: endpoint
      value: s3.amazonaws.com
    - name: secretKeyName
      value: s3-key
    - name: accesskey
      value: AccessKeyId
    - name: secretkey
      value: SecretAccessKey
    - name: workdir
      value: workdir
    - name: genome
      value: genome
    - name: genome-dir
      value: genome
    - name: reference-dir
      value: reference
    - name: threads
      value: 6
    - name: bwa-threads
      value: 14
    - name: dbsnp
      value: mgp.v4.snps.dbSNP.vcf.gz
    - name: indel1
      value: mgp.v4.indels.dbSNP.vcf.gz
    - name: indel2
      value: Mills_and_1000G_gold_standard.indels.hg38.vcf.gz
    - name: targets
      value: Regions.interval_list
    - name:  baits
      value: Covered.interval_list

  templates:
  - name: gatk-pre
    steps:
    - - name: gatk-pre-base
        template: gatk-pre-steps
        arguments:
          parameters:
          - name: uid
            value: "{{item.uid}}"
          - name: R1
            value: "{{item.R1}}"
          - name: R2
            value: "{{item.R2}}"
        withParam: "{{workflow.parameters.sample-list}}"


  - name: gatk-pre-steps
    inputs:
      parameters:
        - name: uid
        - name: R1
        - name: R2

    steps:
    - - name: bwa # add to this - artifact S3 repo, input/output in S3 repo
        template: bwa-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"
          - name: R1
            value: "{{inputs.parameters.R1}}"
          - name: R2
            value: "{{inputs.parameters.R2}}"
          - name: threads
            value: "{{workflow.parameters.bwa-threads}}"


    - - name: picard-readgroups  # now sort by query-name, next step sorts by position after but works better/faster with queryname
        template: picard-readgroups-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"
          - name: RGID
            value: "{{steps.bwa.outputs.parameters.RGID}}"


    - - name: MarkDuplicatesSpark
        template: MarkDuplicatesSpark-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"
          - name: threads
            value: "{{workflow.parameters.threads}}"

    - - name: BaseRecal
        template: BaseRecal-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"

    - - name: ApplyBQSR
        template: ApplyBQSR-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"


    - - name: picard-metrics
        template: picard-metrics-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"

    - - name: haplotypecaller
        template: haplotypecaller-template
        arguments:
          parameters:
          - name: uid
            value: "{{inputs.parameters.uid}}"    


  - name: bwa-template

    retryStrategy:
      retryPolicy: Always
      limit: 5
      backoff:
        duration: 1m
        factor: 2
        maxDuration: 8m

    inputs:
      parameters:
      - name: uid
      - name: R1
      - name: R2
      - name: threads
      
      artifacts:
      - name: R1
        path: /data/R1.fastq.gz
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{inputs.parameters.R1}}"
          region: "us-east-1"

      - name: R2
        path: /data/R2.fastq.gz
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{inputs.parameters.R2}}"
          region: "us-east-1"

      - name: genome-bucket
        path: /data/genome
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.genome-dir}}.tgz"
          region: "us-east-1"

    outputs:
      artifacts:
      - name: bucket-out
        path: /data/aligned.sam
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/aligned.sam"
          region: "us-east-1"

      parameters:
      - name: RGID
        valueFrom:
          path: /data/RGID.txt

    script:
      image: dcibioinformatics/bwa
      command: [bash]
      source: |
        bwa mem -aM -t {{inputs.parameters.threads}} /data/genome/{{workflow.parameters.genome}} /data/R1.fastq.gz /data/R2.fastq.gz > /data/aligned.sam && zcat /data/R1.fastq.gz | head -n 1 | awk '{match($0, /^@\W*([a-zA-Z0-9_:]+)/, m); split(m[1], n, ":"); print n[3]"."n[4]}' > /data/RGID.txt

      volumeMounts:
      - mountPath: /data
        name: data

      resources:
        requests:
          ephemeral-storage: 40Gi
        limits:
          ephemeral-storage: 80Gi
          memory: 22Gi
          cpu: 12


  - name: picard-readgroups-template
  
    retryStrategy:
      retryPolicy: Always
      limit: 5
      backoff:
        duration: 1m
        factor: 2
        maxDuration: 8m

    inputs:
      parameters:
      - name: uid
      - name: RGID
      
      artifacts:
      - name: aligned
        path: /data/aligned.sam
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/aligned.sam"
          region: "us-east-1"

    outputs:
      artifacts:
      - name: sorted
        path: /data/sorted.bam
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/sorted.bam"
          region: "us-east-1"

    script:
      image: broadinstitute/picard
      command: [bash]
      source: |
        java -jar /usr/picard/picard.jar AddOrReplaceReadGroups I=/data/aligned.sam O=/data/sorted.bam RGID={{inputs.parameters.RGID}} RGPL=Illumina RGSM={{inputs.parameters.uid}} RGPU={{inputs.parameters.RGID}}.{{inputs.parameters.uid}} RGLB={{inputs.parameters.uid}} VALIDATION_STRINGENCY=STRICT CREATE_INDEX=true SO=queryname

      volumeMounts:
      - mountPath: /data
        name: data

      resources:
        requests:
          ephemeral-storage: 40Gi
        limits:
          ephemeral-storage: 90Gi
          memory: 22Gi
          cpu: 6


  - name: MarkDuplicatesSpark-template

    retryStrategy:
      retryPolicy: Always
      limit: 5
      backoff:
        duration: 1m
        factor: 2
        maxDuration: 8m

    inputs:
      parameters:
      - name: uid
      - name: threads

      artifacts:
      - name: bucket-in
        path: /data/sorted.bam
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/sorted.bam"
          region: "us-east-1"

    outputs:
      artifacts:
      - name: marked
        path: /data/marked.bam
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/marked.bam"
          region: "us-east-1"

      - name: marked-bai
        path: /data/marked.bam.bai
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/marked.bam.bai"
          region: "us-east-1"

      - name: marked-sbi
        path: /data/marked.bam.sbi
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/marked.bam.sbi"
          region: "us-east-1"

      - name: metrics
        path: /data/dup_metrics.txt
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/dup_metrics.txt"
          region: "us-east-1"

    script:
      image: broadinstitute/gatk:4.1.9.0
      command: [bash]
      source: |
        gatk MarkDuplicatesSpark -I /data/sorted.bam -O /data/marked.bam -M /data/dup_metrics.txt --conf 'spark.executor.cores={{inputs.parameters.threads}}'

      volumeMounts:
      - mountPath: /data
        name: data

      resources:
        requests:
          ephemeral-storage: 40Gi
        limits:
          ephemeral-storage: 90Gi
          memory: 22Gi
          cpu: 6


  - name: BaseRecal-template
  
    retryStrategy:
      retryPolicy: Always
      limit: 5
      backoff:
        duration: 1m
        factor: 2
        maxDuration: 8m

    inputs:
      parameters:
      - name: uid

      artifacts:
      - name: marked
        path: /data/marked.bam
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/marked.bam"
          region: "us-east-1"

      - name: marked-bai
        path: /data/marked.bam.bai
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/marked.bam.bai"
          region: "us-east-1"

      - name: marked-sbi
        path: /data/marked.bam.sbi
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/marked.bam.sbi"
          region: "us-east-1"

      - name: genome-bucket
        path: /data/genome
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.genome-dir}}.tgz"
          region: "us-east-1"

      - name: reference-bucket
        path: /data/reference
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.reference-dir}}.tgz"
          region: "us-east-1"

    outputs:
      artifacts:
      - name: recal-table
        path: /data/recal_table.txt
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/recal_table.txt"
          region: "us-east-1"

    script:
      image: broadinstitute/gatk:4.1.9.0
      command: [bash]
      source: |
        gatk BaseRecalibrator -I /data/marked.bam --known-sites /data/reference/{{workflow.parameters.dbsnp}} --known-sites /data/reference/{{workflow.parameters.indel1}} --known-sites /data/reference/{{workflow.parameters.indel2}} -O /data/recal_table.txt -R /data/genome/{{workflow.parameters.genome}}

      volumeMounts:
      - mountPath: /data
        name: data

      resources:
        requests:
          ephemeral-storage: 40Gi
        limits:
          ephemeral-storage: 90Gi
          memory: 22Gi
          cpu: 6


  - name: ApplyBQSR-template

    retryStrategy:
      retryPolicy: Always
      limit: 5
      backoff:
        duration: 1m
        factor: 2
        maxDuration: 8m

    inputs:
      parameters:
      - name: uid
      
      artifacts:
      - name: bucket-in
        path: /data/marked.bam
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/marked.bam"
          region: "us-east-1"

      - name: genome-bucket
        path: /data/genome
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.genome-dir}}.tgz"
          region: "us-east-1"

      - name: recal-table
        path: /data/recal_table.txt
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/recal_table.txt"
          region: "us-east-1"

    outputs:
      artifacts:
      - name: bucket-out
        path: /data/recal/
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/recal.tgz"
          region: "us-east-1"


    script:
      image: broadinstitute/gatk:4.1.9.0
      command: [bash]
      source: |
        mkdir /data/recal
        gatk ApplyBQSR -I /data/marked.bam -O /data/recal/recal.bam -R /data/genome/{{workflow.parameters.genome}} -bqsr /data/recal_table.txt -OBI

      volumeMounts:
      - mountPath: /data
        name: data

      resources:
        requests:
          ephemeral-storage: 40Gi
        limits:
          ephemeral-storage: 90Gi
          memory: 22Gi
          cpu: 6


  - name: picard-metrics-template

    retryStrategy:
      retryPolicy: Always
      limit: 5
      backoff:
        duration: 1m
        factor: 2
        maxDuration: 8m

    inputs:
      parameters:
      - name: uid
      
      artifacts:
      - name: recal
        path: /data/recal/
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/recal.tgz"
          region: "us-east-1"

      - name: genome-bucket
        path: /data/genome
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.genome-dir}}.tgz"
          region: "us-east-1"

      - name: reference-bucket
        path: /data/reference
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.reference-dir}}.tgz"
          region: "us-east-1"

    outputs:
      artifacts:
      - name: metrics
        path: /data/metrics.txt
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/metrics.txt"
          region: "us-east-1"

    script:
      image: broadinstitute/picard
      command: [bash]
      source: |
        java -jar /usr/picard/picard.jar CollectHsMetrics R=/data/genome/{{workflow.parameters.genome}} TARGET_INTERVALS=/data/genome/{{workflow.parameters.targets}} BAIT_INTERVALS=/data/genome/{{workflow.parameters.baits}} I=/data/recal/recal.bam O=/data/metrics.txt

      volumeMounts:
      - mountPath: /data
        name: data

      resources:
        requests:
          ephemeral-storage: 40Gi
        limits:
          ephemeral-storage: 90Gi
          memory: 22Gi
          cpu: 6


  - name: haplotypecaller-template

    retryStrategy:
      retryPolicy: Always
      limit: 5
      backoff:
        duration: 1m
        factor: 2
        maxDuration: 8m

    inputs:
      parameters:
      - name: uid

      artifacts:
      - name: recal
        path: /data/recal/
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/recal.tgz"
          region: "us-east-1"

      - name: genome-bucket
        path: /data/genome
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.genome-dir}}.tgz"
          region: "us-east-1"

      - name: reference-bucket
        path: /data/reference
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.reference-dir}}.tgz"
          region: "us-east-1"

    outputs:
      artifacts:
      - name: vcf
        path: /data/output.g.vcf
        s3:
          bucket: "{{workflow.parameters.bucket}}"
          endpoint: "{{workflow.parameters.endpoint}}"
          accessKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.accesskey}}"
          secretKeySecret:
            name: "{{workflow.parameters.secretKeyName}}"
            key: "{{workflow.parameters.secretkey}}"
          key: "{{workflow.parameters.workdir}}/{{inputs.parameters.uid}}/output.g.vcf"

    script:
      image: broadinstitute/gatk:4.1.9.0
      command: [bash]
      source: |
        gatk HaplotypeCaller -I /data/recal/recal.bam -O /data/output.g.vcf -ERC GVCF -R /data/genome/{{workflow.parameters.genome}} -D /data/reference/{{workflow.parameters.dbsnp}} -L /data/genome/{{workflow.parameters.targets}}

  volumes:
  - name: data
    emptyDir: {}
