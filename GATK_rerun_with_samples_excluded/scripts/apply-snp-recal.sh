#!/bin/bash

wdir=/mnt/data1/workspace/Cooney/WES/Proc/gatk_wdir/

singularity exec -B ${wdir}:/opt/wdir/:rw docker://broadinstitute/gatk:4.1.9.0 bash -c 'gatk ApplyVQSR -V /opt/wdir/indel_recal.vcf.gz --recal-file /opt/wdir/snps.recal.vcf --tranches-file /opt/wdir/snps.tranches --truth-sensitivity-filter-level 99.7 -mode SNP -OVI -O /opt/wdir/final.vcf.gz'
