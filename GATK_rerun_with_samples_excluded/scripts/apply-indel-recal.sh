#!/bin/bash

wdir=/mnt/data1/workspace/Cooney/WES/Proc/gatk_wdir/

singularity exec -B ${wdir}:/opt/wdir/:rw docker://broadinstitute/gatk:4.1.9.0 bash -c 'gatk ApplyVQSR -V /opt/wdir/filtered.vcf.gz --recal-file /opt/wdir/indels.recal.vcf --tranches-file /opt/wdir/indels.tranches --truth-sensitivity-filter-level 99.7 -mode INDEL -OVI -O /opt/wdir/indel_recal.vcf.gz'
