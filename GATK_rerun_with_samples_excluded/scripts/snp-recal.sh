#!/bin/bash
genome=/mnt/data1/workspace/Cooney/WES/Annotation/genome/
reference=/mnt/data1/workspace/Cooney/WES/Annotation/reference/
wdir=/mnt/data1/workspace/Cooney/WES/Proc/gatk_wdir/

singularity exec -B ${wdir}:/opt/wdir/:rw -B ${reference}:/reference/:ro docker://broadinstitute/gatk:4.1.9.0 bash -c 'gatk VariantRecalibrator -V /opt/wdir/sites.vcf.gz --trust-all-polymorphic -tranche 100.0 -tranche 99.95 -tranche 99.9 -tranche 99.8 -tranche 99.6 -tranche 99.5 -tranche 99.4 -tranche 99.3 -tranche 99.0 -tranche 98.0 -tranche 97.0 -tranche 90.0 -an QD -an MQ -an MQRankSum -an ReadPosRankSum -an FS -an SOR -an InbreedingCoeff -mode SNP --max-gaussians 1 -resource:hapmap,known=false,training=true,truth=true,prior=15.0 /reference/hapmap_3.3.hg38.vcf.gz -resource:omni,known=false,training=true,truth=true,prior=12.0 /reference/1000G_omni2.5.hg38.vcf.gz -resource:1000G,known=false,training=true,truth=false,prior=10.0 /reference/1000G_phase1.snps.high_confidence.hg38.vcf.gz -resource:dbsnp,known=true,training=false,truth=false,prior=2.0 /reference/Homo_sapiens_assembly38.dbsnp138.vcf -O /opt/wdir/snps.recal.vcf --tranches-file /opt/wdir/snps.tranches'
